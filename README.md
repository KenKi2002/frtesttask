###API:
- api/v1/auth/client/ to create client
- api/v1/auth/client/{pk}/ to update or delete client
- api/v1/mailing/ to create or get list of obj mailing with info about messages
- api/v1/mailing/{pk}/ to delete or update obj mailing
- api/v1/mailing/mailing-messages/{pk}/ to get info about mailing with all messages

###Дополнительные задания:

1. Подготовлен docker-compose файл
2. Был добавлен страница со Swagger UI по адресу api/v1/docs/
