from rest_framework import serializers
from . import models


class ClientSerializer(serializers.ModelSerializer):
    class Meta:
        model = models.Client
        fields = '__all__'

    def validate_telephone_number(self, value):
        """
        Check that the number was correctly.
        """
        if value[0] != '7':
            raise serializers.ValidationError("This number dont supported with our service")

        if len(value) < 11:
            raise serializers.ValidationError("Incorrect number")
        try:
            int(value)
        except ValueError:
            raise serializers.ValidationError("Incorrect number")
        return value
