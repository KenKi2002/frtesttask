# Generated by Django 4.0.1 on 2022-01-25 18:31

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('oauth', '0002_remove_client_mailings'),
    ]

    operations = [
        migrations.CreateModel(
            name='Message',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('datetime', models.DateTimeField(auto_now=True)),
                ('status', models.CharField(choices=[('sent', 'sent'), ('failed', 'failed')], max_length=10)),
                ('mailing_id', models.IntegerField()),
                ('client_id', models.IntegerField()),
            ],
        ),
    ]
