from django.contrib import admin
from . import models


@admin.register(models.Message)
class MessageAdmin(admin.ModelAdmin):
    list_display = ('id', 'datetime', 'mailing_id', 'client_id', 'status')
    list_display_links = ('id',)


@admin.register(models.Client)
class MessageAdmin(admin.ModelAdmin):
    list_display = ('id',)
    list_display_links = ('id',)