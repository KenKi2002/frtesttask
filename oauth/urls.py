from django.urls import path

from . import views


urlpatterns = [
    path('client/', views.ClientView.as_view({'post': 'create'})),
    path('client/<int:pk>/', views.ClientView.as_view({'put': 'update', 'delete': 'destroy'})),
]