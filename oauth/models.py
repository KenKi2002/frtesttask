from django.db import models


class Client(models.Model):
    telephone_number = models.CharField(max_length=11)
    tag = models.CharField(max_length=20)
    mobile_operator_code = models.CharField(max_length=3)
    time_zone = models.CharField(max_length=30)


class Message(models.Model):
    STATUS_CHOICES = (
        ('sent', 'sent'),
        ('failed', 'failed')
    )

    datetime = models.DateTimeField(auto_now=True)
    status = models.CharField(max_length=10, choices=STATUS_CHOICES)
    mailing_id = models.IntegerField()
    client_id = models.IntegerField()