from rest_framework import viewsets

from . import serializer, models


class ClientView(viewsets.ModelViewSet):
    """ CUD clients
    """
    queryset = models.Client.objects.all()
    serializer_class = serializer.ClientSerializer


