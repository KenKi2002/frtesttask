from rest_framework import serializers

from . import models
from oauth.models import Message


class MailingSerializer(serializers.ModelSerializer):
    class Meta:
        model = models.Mailing
        fields = '__all__'


class MailingListSerializer(serializers.ModelSerializer):
    sent_mes = serializers.IntegerField()
    failed_mes = serializers.IntegerField()

    class Meta:
        model = models.Mailing
        fields = ('id', 'start_datetime', 'end_datetime', 'message', 'tag_filters', 'code_filters', 'sent_mes', 'failed_mes')


class MessageListSerializer(serializers.ModelSerializer):
    class Meta:
        model = Message
        fields = '__all__'
