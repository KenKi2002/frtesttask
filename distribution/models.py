import datetime
from django.utils import timezone

from django.db import models

from .tasks import send_messages


class Mailing(models.Model):
    """
    tag_filters: indicate in the form 'tag tag tag ...'
    code_filters: indicate in the form 'code code code ...'
    """
    start_datetime = models.DateTimeField()
    message = models.TextField(max_length=2000)
    end_datetime = models.DateTimeField()
    tag_filters = models.CharField(max_length=200, blank=True, null=True)
    code_filters = models.CharField(max_length=200, blank=True, null=True)

    def save(self, force_insert=False, force_update=False, using=None,
             update_fields=None):
        # noinspection PyCompatibility
        super().save()
        now = timezone.now()
        tag_filters = self.tag_filters.split(' ') if self.tag_filters is not None else None
        code_filters = self.code_filters.split(' ') if self.code_filters is not None else None
        if self.start_datetime < now < self.end_datetime:
            send_messages.apply_async((tag_filters, code_filters, self.message, self.id),
                                      expires=self.end_datetime)
        elif now < self.start_datetime:
            send_messages.apply_async((self.tag_filters, self.code_filters, self.message, self.id),
                                      countdown=self.start_datetime,
                                      expires=self.end_datetime)
