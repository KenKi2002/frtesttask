from django.urls import path

from . import views


urlpatterns = [
    path('', views.MailingView.as_view({'post': 'create', 'get': 'list'})),
    path('<int:pk>/', views.MailingView.as_view({'put': 'update', 'delete': 'destroy'})),
    path('mailing-messages/<int:pk>/', views.MessageListView.as_view()),
]
