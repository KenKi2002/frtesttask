from django.contrib import admin

from . import models


@admin.register(models.Mailing)
class MailingAdmin(admin.ModelAdmin):
    list_display = ('id', 'start_datetime', 'message', 'end_datetime', 'tag_filters', 'code_filters')
    list_display_links = ('id',)
