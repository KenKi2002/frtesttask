from FRTestTask.celery import app
from oauth.models import Client
from .services import send_message_to, create_messages


@app.task
def send_messages(tag_filters, code_filters, message, mailing_id):
    if tag_filters is not None and code_filters is not None:
        clients = Client.objects.filter(tag__in=tag_filters, mobile_operator_code__in=code_filters)
        create_messages(clients, mailing_id)
        send_message_to(clients, message, mailing_id)
    elif tag_filters is None and code_filters is not None:
        clients = Client.objects.filter(mobile_operator_code__in=code_filters)
        create_messages(clients, mailing_id)
        send_message_to(clients, message, mailing_id)
    elif tag_filters is not None and code_filters is None:
        clients = Client.objects.filter(tag__in=tag_filters)
        create_messages(clients, mailing_id)
        send_message_to(clients, message, mailing_id)
    else:
        clients = Client.objects.all()
        create_messages(clients, mailing_id)
        send_message_to(clients, message, mailing_id)
