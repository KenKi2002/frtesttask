from django.conf import settings
from oauth.models import Message
import requests


def send_message_to(clients, text, mailing_id):
    for client in clients:
        message = Message.objects.get(mailing_id=mailing_id,
                                      client_id=client.id,
                                      status='created')
        url = f'https://probe.fbrq.cloud/v1/send/{message.id}'
        data = {'id': client.id,
                'phone': int(client.telephone_number),
                'text': text}
        res = requests.post(url, headers={'Authorization': f'{settings.JWT_MAIL_TOKEN}'}, data=data)
        if res.status_code == 400:
            message.status = 'failed'
            message.save()
        else:
            message.status = 'sent'
            message.save()


def create_messages(clients, mailing_id):
    for client in clients:
        message = Message.objects.create(mailing_id=mailing_id,
                                         client_id=client.id,
                                         status='created')
        message.save()
