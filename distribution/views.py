from django.db.models import Count, F
from rest_framework import viewsets, generics
from rest_framework.response import Response

from . import serializers, models
from oauth.models import Message


class MailingView(viewsets.ModelViewSet):
    """ CRUD mailings
    """
    queryset = models.Mailing.objects.all()
    serializer_class = serializers.MailingSerializer

    def list(self, request, *args, **kwargs):
        queryset = models.Mailing.objects.annotate(sent_mes=Count(
                                                        Message.objects.filter(mailing_id=F('id'),
                                                                               status='sent').count()),
                                                   failed_mes=Count(
                                                       Message.objects.filter(mailing_id=F('id'),
                                                                              status='failed').count())
                                                   )

        page = self.paginate_queryset(queryset)
        if page is not None:
            serializer = serializers.MailingListSerializer(page, many=True)
            return self.get_paginated_response(serializer.data)

        serializer = serializers.MailingListSerializer(queryset, many=True)
        return Response(serializer.data)


class MessageListView(generics.ListAPIView):
    serializer_class = serializers.MessageListSerializer

    def list(self, request, *args, **kwargs):
        queryset = Message.objects.filter(mailing_id=kwargs['pk'])

        page = self.paginate_queryset(queryset)
        if page is not None:
            serializer = self.get_serializer(page, many=True)
            return self.get_paginated_response(serializer.data)

        serializer = self.get_serializer(queryset, many=True)
        return Response(serializer.data)
